<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix'=>'users'], function(){
    Route::resource('crud', 'UserController');
    Route::put('crudblacklist/{crud}', 'UserController@updateBlacklist');
});
Route::group(['prefix'=>'buildings'],function(){
	Route::resource('crud','BuildingController');
});
Route::group(['prefix'=>'user_buildings'],function(){
	Route::post('new','UserController@asignaEdificio');
	//El update por tiempo no alcanzo a hacerlo...
	//Route::update('update','UserController@updateUserBuilding');
	Route::delete('delete','UserController@deleteUserBuilding');
});
