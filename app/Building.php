<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $fillable = [
        'name', 'address', 'floors_total',
    ];


    public function users()
    {
        return $this->belongsToMany(User::class,'user_building');
    }
}
