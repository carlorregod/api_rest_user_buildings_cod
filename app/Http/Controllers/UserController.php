<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Llamando al modelo de los user
use App\User;
use App\Building;
//Request, se decide no usarlo y, en su lugar, usar validadores
//use App\Http\Requests\UserRequest, 
use Validator;

class UserController extends Controller
{
    //Métodos personales, para efectuar pre-validaciones
    private static function validandoFormulario($request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6'
        ]);
        return $validate;
    }
    private static function validandoFormularioUpdateNoPassword($request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users'
        ]);

        return $validate;
    }
    private static function validandoFormularioUpdateBlacklist($request)
    {
        $validate = Validator::make($request->all(), [
            'blacklist' => 'required|boolean',
        ]);

        return $validate;
    }
    private static function validandoFormularioUsuarioEdificio($request)
    {
        $validate = Validator::make($request->all(), [
            'user_id' => 'required|integer|min:1',
            'building_id' => 'required|integer|min:1',
            'floor_number' => 'required|integer|min:1',
        ]);

        return $validate;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::select('id','name','email','blacklist')->get();
        if(count($usuarios) == 0){
            return response()->json(['id'=>null,'name'=>'','email'=>'','blacklist'=>false],201);
        }
        return response()->json($usuarios,201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //Crear por get, se usará el create por POST
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validate =self::validandoFormulario($request);

        if(!$validate->fails()){

            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'blacklist' => false,
            ]);
            return response()->json(['status'=>0,'mensaje'=>'Registro creado exitosamente','nombre'=>$request->name],200);
        }else{
            return response()->json(['status'=>1,'mensaje'=>'Data a insertar reprueba validación. Revise sus campos, el email podría estar en uso o su contraseña no respeta la longítud mínima de 6 caracteres y que sea alfanumérica'],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $usuario = User::where('id',$id)->first();
        $usuario->buildings;
        return response()->json($usuario);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Se usará update
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $validate =self::validandoFormularioUpdateNoPassword($request);

            if(!$validate->fails()){
                //Revisando si existe el id
                $revisar_id = User::where('id',$id)->select('id')->first();
                //Si no existe $revisar_id es que no hay registro con la id específica
                if(isset($revisar_id->id)){
                    User::where('id',$id)->update([
                        'name' => $request->name,
                        'email' => $request->email,
                        ]);
                    return response()->json(['status'=>0, 'mensaje'=>'Actualización lista'],200);

                }else{
                    return response()->json(['status'=>1, 'mensaje'=>'Actualización no se lleva a cabo. Id no existe'],203);
                }
            }else{
                return response()->json(['status'=>1,'mensaje'=>'Data a actualizar reprueba validación. Revise sus campos, el email podría estar en uso o su contraseña no respeta la longítud mínima de 6 caracteres y que sea alfanumérica'],203);
            }
            
        }catch(\Exception $e){
            return response()->json(['status'=>1, 'mensaje'=>'Error al actualizar registro: '.$e->getMessage()],203);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            //Revisando si existe el id
            $revisar_id = User::where('id',$id)->select('id')->first();
            if(isset($revisar_id->id)){
                User::where('id',$id)->delete();
                return response()->json(['status'=>0, 'mensaje'=>'Borrado listo'],200);
                
            }else{
                return response()->json(['status'=>0, 'mensaje'=>'Borrado no se efectúa porque el id ingresado no existe en la data'],203);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>1, 'mensaje'=>'Error al eliminar registro: '.$e->getMessage()],203);
        }
        
    }

    //Proceso que solo actualiza el blacklist o lista negra de usuario
    public function updateBlacklist(Request $request, $id)
    {
        try{
            $validate =self::validandoFormularioUpdateBlacklist($request);

            if(!$validate->fails()){
                //Revisando si existe el id
                $revisar_id = User::where('id',$id)->select('id')->first();
                //Si no existe $revisar_id es que no hay registro con la id específica
                if(isset($revisar_id->id)){
                    User::where('id',$id)->update([
                        'blacklist' => $request->blacklist,

                        ]);
                    return response()->json(['status'=>0, 'mensaje'=>'Actualización lista negra ok'],200);

                }else{
                    return response()->json(['status'=>1, 'mensaje'=>'Actualización lista negra no se lleva a cabo. Id no existe'],203);
                }
            }else{
                return response()->json(['status'=>1,'mensaje'=>'Data a actualizar reprueba validación. La lista negra debe ser un valor true o false'],203);
            }
            
        }catch(\Exception $e){
            return response()->json(['status'=>1, 'mensaje'=>'Error al actualizar registro: '.$e->getMessage()],203);
        }
    }
    /**
     * Asignando un edificio al usuario. Se pasa el id del edificio
     *
     * @param  int  $user_id
     * @param  int  $building_id
     * @return \Illuminate\Http\Response
     */
    public function asignaEdificio(Request $request){        
        try{
            $validate =self::validandoFormularioUsuarioEdificio($request);

            if(!$validate->fails()){

                //Prevalidaciones ¿Existe el $user_id en la tabla de users?
                $usuario = User::find($request->user_id);
                //Viendo el tema de la blacklist. Un usuario en lista negra no podrá asignarse edificio
                if(!isset($usuario->blacklist)){
                    $blacklist = "0";
                }else{
                    $blacklist = $usuario->blacklist;
                }

                //Idem para el building
                $edificio = Building::find($request->building_id);
                //Revisando si ya existe previamente el user y el building
                $user_building = \DB::table('user_building')->where('user_id',$request->user_id)->where('building_id',$request->building_id)->where('floor_number',$request->floor_number)->first();
                $piso = isset($user_building->floor_number) ? intval($user_building->floor_number) : 0;             
                $logica_piso = $piso == $request->floor_number; //Dará true si el piso está ocupado por el user en el depto preasignado

                if($logica_piso){
                    return response()->json(['status'=>1, 'mensaje'=>'Usuario ya tiene asignado el edificio en el piso'],203);
                }
                //Existe en la BD el usuario y el edificio???
                if($usuario != null && $edificio != null)
                {                    
                    if($user_building == null){
                            //Blacklist cero no insertará

                            if($blacklist == "0"){
                                return response()->json(['status'=>1, 'mensaje'=>'Edificio-piso no asignado a usuario porque éste último se encuentra en la lista negra'],203); 

                            }else{

                                \DB::table('user_building')->insert([
                                'user_id'=>$request->user_id,
                                'building_id'=>$request->building_id,
                                'floor_number'=>$request->floor_number,
                                'created_at'=>now(),
                                'updated_at'=>now(),

                                ]);
                                return response()->json(['status'=>0, 'mensaje'=>'Edificio-piso asignado exitosamente a usuario'],200); 
                            }




                   }else{
                        return response()->json(['status'=>1, 'mensaje'=>'Edificio-piso ya estaba asignado a usuario'],203); 
                   }

                               
                }else{
                    return response()->json(['status'=>1,'mensaje'=>'El id ingresado para el usuario y/o el edificio no existen en la data'],203);
                }
            }else{        
                return response()->json(['status'=>1,'mensaje'=>'Data a actualizar reprueba validación. Recuerde que los campos requeridos deben ser de tipo entero para user_id, building_id y floor_number'],203);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>1, 'mensaje'=>'Error al generar la relación de usuario y edificio: '.$e->getMessage()],203);
        }
    }
    /**
     * Eliminando asignación de edificio al usuario. Se pasa el id del usuario y edificio
     *
     * @param  int  $user_id
     * @param  int  $building_id
     * @return \Illuminate\Http\Response
     */
    public function deleteUserBuilding(Request $request){
        try{
             \DB::table('user_building')->where('user_id',$request->user_id)->where('building_id',$request->building_id)->where('floor_number',$request->floor_number)->delete();
             return response()->json(['status'=>0, 'mensaje'=>'Borrado listo de relación usuario y edificio'],200);
        }catch(\Exception $e){
             return response()->json(['status'=>1, 'mensaje'=>'Error al borrar relación de usuario y edificio: '.$e->getMessage()],203);
        }
    }

}
