<?php

namespace App\Http\Controllers;

use App\Building;
use Illuminate\Http\Request;

class BuildingController extends Controller
{
    //Métodos personales, para efectuar pre-validaciones
    private static function validandoFormulario($request)
    {
        $validate = \Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'floors_total' => 'required|integer|min:1'
        ]);
        return $validate;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $edificios = Building::select('id','name','address','floors_total')->get();
        if(count($edificios) == 0){
            return response()->json(['id'=>null,'name'=>'','address'=>'','floors_total'=>0],201);
        }
        return response()->json($edificios,201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate =self::validandoFormulario($request);

        if(!$validate->fails()){

            Building::create([
                'name' => $request->name,
                'address' => $request->address,
                'floors_total' => $request->floors_total,
            ]);
            return response()->json(['status'=>0,'mensaje'=>'Registro creado exitosamente','nombre'=>$request->name],200);
        }else{
            return response()->json(['status'=>1,'mensaje'=>'Data a insertar reprueba validación. Revise sus campos, name y address son cadenas de texto y floors_total número entero positivo'],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Building  $building
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $edificio = Building::where('id',$id)->first();
        $edificio->users;
        return response()->json($edificio);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Building  $building
     * @return \Illuminate\Http\Response
     */
    public function edit(Building $building)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Building  $building
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try{
            $validate =self::validandoFormulario($request);

            if(!$validate->fails()){
                //Revisando si existe el id
                $revisar_id = Building::where('id',$id)->select('id')->first();
                //Si no existe $revisar_id es que no hay registro con la id específica
                if(isset($revisar_id->id)){
                    Building::where('id',$id)->update([
                        'name' => $request->name,
                        'address' => $request->address,
                        'floors_total' => $request->floors_total,
                        ]);
                    return response()->json(['status'=>0, 'mensaje'=>'Actualización lista'],200);

                }else{
                    return response()->json(['status'=>1, 'mensaje'=>'Actualización no se lleva a cabo. Id no existe'],203);
                }
            }else{
                return response()->json(['status'=>1,'mensaje'=>'Data a insertar reprueba validación. Revise sus campos, name y address son cadenas de texto y floors_total número entero positivo'],203);
            }
            
        }catch(\Exception $e){
            return response()->json(['status'=>1, 'mensaje'=>'Error al actualizar registro: '.$e->getMessage()],203);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Building  $building
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            //Revisando si existe el id
            $revisar_id = Building::where('id',$id)->select('id')->first();
            if(isset($revisar_id->id)){
                Building::where('id',$id)->delete();
                return response()->json(['status'=>0, 'mensaje'=>'Borrado listo'],200);
                
            }else{
                return response()->json(['status'=>0, 'mensaje'=>'Borrado no se efectúa porque el id ingresado no existe en la data'],203);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>1, 'mensaje'=>'Error al eliminar registro: '.$e->getMessage()],203);
        }
    }
}
